# Monitoring (and Logging)

> Sofiane Benahmed
> Ivan Pidikseev

## Overview

In this project, we took a look at the two main motniroring solutions Zabbix and Elasticsearch. We show how to deploy them and configure them and their agent on a remote system. Then we show how they perform the alerting and monitoring of the metrics that agents are watching. For ELK, we show how to setup an application monitoring agent on NodeJS.

## I. Zabbix

### 1. Installation

#### Package

There are multiple ways of installing Zabbix and one of them is using repositories. To install, we will add its repository:

```bash
wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-2+xenial_all.deb
dpkg -i zabbix-release_4.0-2+xenial_all.deb
apt update
```

On the machine that needs to be monitored, we will install the Zabbix Agent so that it would send data to the Zabbix Server:

```bash
apt install zabbix-agent 
```

On another machine we will install the Zabbix Server:

```bahs
zabbix-server-mysql zabbix-frontend-php 
```


#### MySQL

It requires a MySQL database, we install it:

```bash
mysql -uroot -p
create database zabbix character set utf8 collate utf8_bin;
grant all privileges on zabbix.* to zabbix@localhost identified by 'password';
quit;
```

Creating the tables for the database:

```bash
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p zabbix
```

Then we edit the file */etc/zabbix/zabbix_server.conf* and add `DBPassword=password`.


#### PHP Timezone

It also requires setting up a timezone in PHP. To do it, we edit the file `/etc/zabbix/apache.conf` and add `php_value date.timezone Europe/Moscow`. We also edit `/etc/php/7.0/apache2/php.ini` and set `date.timezone = Europe/Moscow`.


```bash
service zabbix-server start
update-rc.d zabbix-server enable
service zabbix-agent start
```


### 2. Configuration 

To setup Zabbix, we should visit the address `http://127.0.0.1/zabbix`:

![](img/zabbix-setup.png)

Everything is configured:

![](img/zabbix-check-reqs.png)

Preinstallation summary:

![](img/zabbix-pre-installation-summary.png)

Default login passwords are `Admin` and `password`. Now we can enter the panel:

![](img/zabbix-dashboard.png)

#### Server configurations

We configure the server as follows:

```ini
ListenPort=10051
LogFile=/var/log/zabbix/zabbix_server.log
LogFileSize=0
DebugLevel=5
PidFile=/var/run/zabbix/zabbix_server.pid
SocketDir=/var/run/zabbix
DBName=zabbix
DBUser=zabbix
DBPassword=password
SNMPTrapperFile=/var/log/snmptrap/snmptrap.log
ListenIP=0.0.0.0
Timeout=4
AlertScriptsPath=/usr/lib/zabbix/alertscripts
ExternalScripts=/usr/lib/zabbix/externalscripts
FpingLocation=/usr/bin/fping
Fping6Location=/usr/bin/fping6
LogSlowQueries=3000
```

#### Agent configurations

The only configurations for the agent are related to Zabbix server and host:

```ini
PidFile=/var/run/zabbix/zabbix_agentd.pid
LogFile=/var/log/zabbix/zabbix_agentd.log
LogFileSize=0
DebugLevel=3
Server=10.1.1.148
ServerActive=10.1.1.148
Hostname=NodeServer
Include=/etc/zabbix/zabbix_agentd.d/*.conf
```

#### Notification Configuration

To set up notifications, we need to create an action with the appropriate conditions. We add an acction for our host and, for the `trigger severity` higher or equals to `Average`:

![](img/zabbix-action-settings.png)

Zabbix already has predefined templates for servers with triggers and we used the `Linux OS` one. The whole list of triggers looks like:

![](img/zabbix-trigger-list.png)

We disabled a standard trigger for processor-load as it calculates with our processor count. So, we added a new one that would be triggered whenever the load is more than `0.8`. With this trigger, we know that either somebody is attacking us or something went wrong as the load is very high. It should not be as high because there must be always spare processor power that would be used in case of attacks or in times of much higher load. For example, if it is an entertaining service, the load on weekends is much higher. And it will be much higher in case of some external factors, like ads.

Also, there was a predefined trigger for the lack of available memory so that if there are memory leaks, we could prevent it. And if the Zabbix agent is not available, which probably means that the server is down, Zabbix will notify about it as it has an appropriate severity for it.

The rest of the metrics are the system ones, for example, about server restarts and it is good to know when it happened and other information about it. But they are not critical and mostly are triggered by the system administrator's actions, that is why it is not necessary to notify about them.

We changed media type to Jabber:

![](img/zabbix-jabber-changed-media-type.png)

added a new media type for the current user:

![](img/zabbix-new-media-for-user.png)

*(it is not necessary to change the severity here as it is already configured in actions)*

and we get notifications when a trigger is fired:

![](img/zabbix-jabber-notifications.png)

There are also notifications about when the problem starts and when it is resolved. In the example above, there were notifications that the Zabbix agent was unreachable, and ones about the high load.

---

## II. ELK

### 1. Installation

To make our life easier, we decided to install ELK using Docker. The fastest way to do that is to use the docker compose which was made for this:

```bash
git clone https://github.com/deviantony/docker-elk.git 
cd docker-elk
docker-compose up -d
```

The result is the following docker instances:
- Elasticsearch
- Logstash
- Kibana

We can check the installation by trying to access the Kibana Dashboard:

![kibana-works](img/kibana-dash-install.png)


### 2. Agent Configuration

In order to get the metrics from our VM, we install the `metricbeat` agent as follows:

```bash
curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-6.6.0-amd64.deb
sudo dpkg -i metricbeat-6.6.0-amd64.deb
```

Then we configure it to connect to the server on the right IP and port and we define which metrics we want from the service. To do this, we edit the file `/etc/metricbeat/metricbeat.yml`. In our case, we configured it to send the data about:
- CPU
- Memory
- Network
- FileSystem
- Processes

And configure it to connect to the elasticsearch server on `<serverIP>:9200`. This configuration can be found in *files/metricbeat.yaml*.

The final step to do on the host was to add the predefined dashboards for Kibana from the metricbeat agent. This can be done using the following command:

```bash
sudo metricbeat setup -E \
  setup.kibana.host={{KibanaServerIP}}:5601 
```

![load-d](img/load-dashes.png)


### 3. Dash Configuration

Once the agent is set-up and the dashboards loaded, we can start configuring Kibana. First, we access Kibana from the server on `localhost:5601`. Then, we go to *Management* then *Index Pattern*. There we add the index pattern for the metrics as follows:

![index-1](img/index-add.png)

![index-2](img/metric-add-2.png)

![index-3](img/metric-add-3.png)

![index-4](img/index-created.png)

In this project, we took a look at the two main monitoring solutions Zabbix and Elasticsearch. We show how to deploy them and configure them and their agent on a remote system. Then we show how they perform the alerting and monitoring of the metrics that agents are watching. For ELK, we show how to set up an application monitoring agent on NodeJS.
Once this is done, Elasticsearch starts receiving logs and parsing them. We can now enable access to the dashboard from *Dashboard*. We select *\[Metricbeat System\] Host Overview*.

![metric-dash](img/metrics-dash.png)

The result is:

![h-d](img/host-dash.png)

Here we can see the dashboard giving us all the information specified  previously. Also, we can see the default dash already created the graphs based on that data.


### 4. Business Metrics

As explained in the lab, in some cases, we want to have statistics about the application itself and have more details. To do this, ELK supports the integration with *Application Performance Monitoring* or APM. To integrate it, we needed to add a new docker instance. We used the `docker-compose.yaml` from the repo and added the following service:

```yaml
  apm:
    build:
      context: apm-server/
      args:
        ELK_VERSION: $ELK_VERSION
    volumes:
      - ./apm-server/config/:/usr/share/apm-server/config/apm-server.yaml:ro
    ports:
            - "8200:8200"
    networks:
      - elk
    depends_on:
      - elasticsearch
```

Then we created the directory for it:

```
files/docker-elk/apm-server
├── config                
│   └── apm-server.yaml    -> Setting the Server
└── Dockerfile             -> Using Official image
```

*Note*: The files can be found in *files/docker-elk*

Once this is done we start the new server:

```bash
docker-compose up -d
```

We then test the connection with Kibana and Enable the dash from *Kibana* then *APM* and we find:

![apm-enable](img/apm-dash.png)

Finally, we add the package to the application. To do so, we first add the package:

```bash
cd app
npm install elastic-apm-node
node server.js
```

Then we add the following commands to start the monitoring **in the beginning** of the script (the full script can be found in *files/app/server.js*):

```Javascript
var apm = require('elastic-apm-node').start({
        serverUrl: 'http://10.1.1.160:8200'
});
```

We configure the index similarly to *metricbeats* and we get the *\[APM\] Services*:

![apm-ds](img/apm-dashes.png)

![apm-app](img/apm-app.png)


## III. Load Testing

After setting up Kibana and Zabbix, we started the load. To do so, we used the `autocannon` library. The installation can be found in *files/start-load.sh*. Using that utility we made two tests:

- Connection Heavy: Increase the number of connections
- Duration Heavy: Keep the test for a longer period

The results were:

**Connection Heavy**

```bash
./autocannon.js -d 100 -c 5000 10.1.1.158:8081
```

![autocannon](img/load-1-autostats.png)

![zabbix](img/load-1-zabbix.png)

![metrics](img/load-1-metric-stats.png)

![apm](img/load-1-apm-stats.png)


**Time Heavy**


```bash
./autocannon.js -d 300 -c 1500 10.1.1.158:8081
```

![autocannon](img/load-2-autostats.png)

![zabbix](img/load-2-zabbix.png)

![metrics](img/load-2-metric-stats.png)

![apm](img/load-2-apm-stats.png)


For Alerting on ELK, the main service is premium however, some open source plugins which provide similar services are available such as [ElastAlert](https://github.com/Yelp/elastalert).


Our web page was a simple one, it only had static resources which are usually processed very fast. For this reason, it did not consume much resources even with the load used during the tests. The results were as we saw from the reports, acceptable with more than 80% of the requests being successful. Our hello world page was not bad) and showed good stamina (it is hard to create an inefficient hello world page and requires a lot of experience (: ).


## Conclusion

The metrics were very accurate on both Zabbix and ELK and showed the system's data with usable and understandable visualizations. Even though the load was high, they were both able to keep the data flow (with the exception of ELK APM which had a full queue -can be configured-) and they kept it until the end of the tests and reflected the actual state quite precisely. 

Highload is a very controversial notion as it depends on the system, platform, the service itself and many other factors. Because of this, the idea of high load is up to the owner of the service. In our case, the load of 5000 parallel clients could be considered as high load since node was not able to consume all the requests as we can see in the autocannon's reports. However, for Google for example, this amount of requests is ridiculously small. For Chinese search engines Yandex's load is not mentionable, however for Yandex it is high load.



## Sources

- https://www.elastic.co/guide/en/beats/metricbeat/master/metricbeat-module-http.html 
- https://elk-docker.readthedocs.io/#forwarding-logs-filebeat
- https://www.elastic.co/guide/en/beats/metricbeat/current/metricbeat-installation.html
- https://www.elastic.co/guide/en/beats/metricbeat/current/metricbeat-template.html
- https://fabianlee.org/2017/04/16/elk-installing-metricbeat-for-collecting-system-and-application-metrics/
- https://logz.io/blog/elk-stack-on-docker/
- https://tecadmin.net/install-zabbix-agent-on-ubuntu-and-debian/
- https://www.elastic.co/guide/en/beats/metricbeat/current/load-kibana-dashboards.html
- https://www.elastic.co/guide/en/beats/devguide/current/import-dashboards.html
- https://www.elastic.co/guide/en/apm/server/current/configuring-ingest-node.html
- https://www.elastic.co/guide/en/apm/agent/nodejs/master/express.html
- https://logz.io/blog/metricbeat-elastic-stack-5-0/
- https://www.zabbix.com/ru/download?zabbix=4.0&os_distribution=ubuntu&os_version=16.04_xenial&db=mysql
- https://www.zabbix.com/documentation/4.0/manual/installation/install_from_packages/debian_ubuntu