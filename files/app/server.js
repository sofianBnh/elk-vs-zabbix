var apm = require('elastic-apm-node').start({
	serverUrl: 'http://10.1.1.160:8200'
});

var express = require('express');
var path = require('path');

var app = express();

app.use(express.static(__dirname + '/img'));
app.get('/',function(req,res) {
	res.sendFile(path.join(__dirname + '/index.html'));
})

console.log("Starting the server")

app.listen(8081);
